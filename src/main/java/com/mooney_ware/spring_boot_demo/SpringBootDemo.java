package com.mooney_ware.spring_boot_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class SpringBootDemo {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(SpringBootDemo.class, args);

        System.out.println("Let's see the beans!");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);

        for (String beanName: beanNames) {
            System.out.println(beanName);
        }
    }
}
